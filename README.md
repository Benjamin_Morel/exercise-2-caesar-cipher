## Exercise 2 – Caesar Cipher

Difficulty: :star: :star: :star:

Implement the [Casear cipher](https://en.wikipedia.org/wiki/Caesar_cipher)
(encryption and decryption):

- the program takes three arguments:

  - the operation (encrypt or decrypt);

  - the key;

  - the name of the file you want to process;

- plaintexts and ciphertexts are text files (all [printable ASCII characters](https://en.wikipedia.org/wiki/ASCII) from `SP` to `∼`);

- the program returns:

  - `0` if encryption (or decryption) is successful;

  - `100` if an IO error occurs;

  - `101` if the key is not valid;  

- write unit tests to validate your encryption and decryption functions;

- package your application into a single JAR file.

---

”On a conservé en outre ses lettres à Cicéron, et celles qu’il adressait à ses
familiers sur ses affaires domestiques; quand il avait à leur faire quelque
communication secrète, il usait d’un chiffre, c’est-à-dire qu’il brouillait les
lettres de telle fac ̧on qu’on ne pût reconstituer aucun mot : si l’on veut en
découvrir le sens et les déchiffrer, il faut substituer à chaque lettre la
troisième qui la suit dans l’alphabet, c’est-à-dire le D à l’A, et ainsi de
suite.”

Suétone, Vie des douze Césars

---
