import Cryptography.Cryptage;
import Storage.FileStorage;
import errors.BusinessException;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;



import static org.junit.jupiter.api.Assertions.*;

class CaesarCipherDemoTest {


    @Test
    public void encryptTest() throws BusinessException {

        File nonEncryptFile = null; //file for not encrypt
        File encryptFile = null;    //file for encrypt


        //create the 2 files nonEncrypt and Encrypt
        try{
            nonEncryptFile = new File("nonEncryptFile.txt");
            encryptFile = new File("encryptFile.txt");
        }
        catch(Exception e){
            fail("cannot create the files");
        }


        //get the path of the 2 files
        Path nonEncryptpath = Paths.get(("nonEncryptFile.txt"));
        Path pathEncrypt = Paths.get(("encryptFile.txt"));


        //create 2 String to write in the different files
        String nonEncryptString = "hello world";
        String encryptString = "ebiil tloia" ;


        //write the String in the file
        try{
            Files.writeString(nonEncryptpath, nonEncryptString, StandardCharsets.UTF_8);
            Files.writeString(pathEncrypt, encryptString, StandardCharsets.UTF_8);
        }catch(Exception ex){
            fail("cannot write in file");
        }


        Cryptage myCrypt = new Cryptage();              //initiate new cryptage
        FileStorage myFileReader = new FileStorage();  //initiate new reader for the file

        myCrypt.encrypt(3,nonEncryptFile);      //encrypt the file


        //get the word from the file
        nonEncryptString = myFileReader.read(nonEncryptFile);
        encryptString = myFileReader.read(encryptFile);


        assertEquals(nonEncryptString,encryptString);


    }

    @Test
    public void decryptTest() throws BusinessException {

        File encryptFile = null;
        File decryptFile = null;


        try{
            encryptFile = new File("encryptFile.txt");
            decryptFile = new File("decryptFile.txt");
        }
        catch(Exception e){
            fail("cannot create the files");
        }


        Path pathEncrypt = Paths.get(("encryptFile.txt"));
        Path pathDecryptFile = Paths.get(("decryptFile.txt"));


        String encryptString = "ebiil tloia";
        String nonEncryptString = "hello world";



        try{
            Files.writeString(pathEncrypt, encryptString, StandardCharsets.UTF_8);
            Files.writeString(pathDecryptFile, nonEncryptString, StandardCharsets.UTF_8);
        }catch(Exception ex){
            fail("cannot write in file");
        }


        Cryptage myCrypt = new Cryptage();
        FileStorage myFileReader = new FileStorage();

        myCrypt.decrypt(3,encryptFile);


        encryptString = myFileReader.read(encryptFile);
        nonEncryptString = myFileReader.read(decryptFile);


        assertEquals(encryptString,nonEncryptString);


    }

}