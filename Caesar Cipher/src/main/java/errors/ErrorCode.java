package errors;

public enum ErrorCode {
    ALL_RIGHT(0),
    IO_ERROR(100),
    KEY_NOTVALID(101);

    private final int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
