package Cryptography;

import Storage.FileStorage;
import errors.BusinessException;

import java.io.File;

public class Cryptage implements CaesarCipher
{
    private char[] alphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    @Override
    public void decrypt(int param, File fileToDecrypt) throws BusinessException {
        char[] alphabetMoved = new char[26];
        char temp;
        FileStorage myFileReadWrite = new FileStorage();
        String myReadedFile = myFileReadWrite.read(fileToDecrypt);

        for(int i =0;i<alphabet.length;i++)
        {
            if(i<param)
            {
                alphabetMoved[i] = alphabet[alphabet.length-param+i];
            }
            else
            {
                alphabetMoved[i] = alphabet[i-param];
            }

        }

        String decryptedString ="";
        for(int i=0;i<myReadedFile.length();i++)
        {
            char myLetter ;
            int myIndex;
            myLetter = myReadedFile.charAt(i);

            if(myLetter != ' ' && myLetter != '\n')
            {
                myIndex = indexOf(myLetter , alphabetMoved);
                decryptedString+=alphabet[myIndex];
            }
            else
            {
                decryptedString+=myLetter;
            }

        }

        myFileReadWrite.write(fileToDecrypt,decryptedString);
    }

    @Override
    public void encrypt(int param, File fileToEncrypt) throws BusinessException
    {
        char[] alphabetMoved = new char[26];
        char temp;
        FileStorage myFileReadWrite = new FileStorage();
        String myReadedFile = myFileReadWrite.read(fileToEncrypt);

        for(int i =0;i<alphabet.length;i++)
        {
            if(i<param)
            {
                alphabetMoved[i] = alphabet[alphabet.length-param+i];
            }
            else
            {
                alphabetMoved[i] = alphabet[i-param];
            }

        }

        String encryptedString ="";
        for(int i=0;i<myReadedFile.length();i++)
        {
            char myLetter ;
            int myIndex;
            myLetter = myReadedFile.charAt(i);

            if(myLetter != ' ' && myLetter != '\n')
            {
                myIndex = indexOf(myLetter,alphabet);
                encryptedString+=alphabetMoved[myIndex];
            }
            else
            {
                encryptedString+=myLetter;
            }

        }

        myFileReadWrite.write(fileToEncrypt,encryptedString);


    }


    public int indexOf(char Letter ,char[] alphabetUsed)
    {
        for(int i=0;i<alphabetUsed.length;i++)
        {
            if(alphabetUsed[i] == Letter)
            {
                return i;
            }
        }
        return -1;
    }
}
