package Cryptography;

import errors.BusinessException;

import java.io.File;

public interface CaesarCipher
{
    public void encrypt(int param , File fileToEncrypt ) throws BusinessException;

    public void decrypt(int param, File fileToDecrypt )throws BusinessException;
}
