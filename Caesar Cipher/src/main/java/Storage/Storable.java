package Storage;

import errors.BusinessException;

import java.io.File;
import java.io.IOException;

public interface Storable
{
    String read(File source) throws BusinessException;

    void write(File destination, String toWrite) throws BusinessException, IOException;
}
