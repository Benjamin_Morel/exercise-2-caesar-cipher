package Storage;

import errors.BusinessException;
import errors.ErrorCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileStorage implements Storable
{
    @Override
    public String read(File source)  {
        String myReadedFile = "";
        try
        {
            Scanner myFile = new Scanner(source);
            while (myFile.hasNextLine())
            {
                myReadedFile += myFile.nextLine();
                myReadedFile += '\n';
            }


        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return myReadedFile;
    }

    @Override
    public void write(File destination, String toWrite) throws BusinessException {
        try
        {
            FileWriter myWriter = new FileWriter(destination);
            myWriter.write(toWrite);
            myWriter.close();
            //throw new BusinessException("succesful ",ErrorCode.ALL_RIGHT);

        }
        catch (IOException e)
        {
            throw new BusinessException("error writing file", ErrorCode.IO_ERROR);

        }

    }
}
